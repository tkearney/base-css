# base.css project

The purpose of this project is to help maintain a base css file to be used in all of my web based projects. 

### Restrictions
I will try and focus on formatting of text and position of text in this code.

### Note from the author
This is not meant be a huge project. I will try and maintain it when I can. I like having this code in a central location where changes can be tracked.

#### Licence
I give permission to anyone who wishes to use this code. I do not own any of the style rules in this project. In some cases, I have borrowed rules from various online articles or forum answers and if you believe any rule is yours, I'll add your name and a link in this document. 
